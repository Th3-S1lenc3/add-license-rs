NPM = pnpm

build: clippy build_binary build_css
	@echo "Build Complete."

build_binary:
	@echo "Building rust binary..."
	@cargo build
	@echo "Done."

build_css:
	@echo "Building css..."
	@${NPM} tailwindcss:build


clippy:
	@echo "Running cargo clippy..."
	@cargo clippy -- -W clippy::pedantic -W clippy::nursery -W clippy::expect_used -W clippy::get_unwrap
