// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use clap::{self, CommandFactory};
use std::ffi::OsString;
use std::fs;
use std::path::{Path, PathBuf};

#[path = "src/cli.rs"]
mod cli;

fn gen_manpage_recursive(cmd: &clap::Command, out_dir: &Path) -> std::io::Result<()> {
    let name = String::from(cmd.get_name());
    let mut buffer: Vec<u8> = Vec::default();
    clap_mangen::Man::new(cmd.clone()).render(&mut buffer)?;
    std::fs::write(out_dir.join(format!("{}.1", name)), buffer)?;

    for subcommand in cmd.get_subcommands() {
        let subcommand_name = subcommand.get_name();
        gen_manpage_recursive(
            &subcommand
                .clone()
                .name(format!("{}-{}", name, subcommand_name)),
            out_dir,
        )?;
    }

    Ok(())
}

fn main() -> std::io::Result<()> {
    let out_dir_env: OsString =
        std::env::var_os("MAN_TARGET_DIR").ok_or(std::io::ErrorKind::NotFound)?;

    let out_dir = PathBuf::from(&out_dir_env);

    if !out_dir.exists() {
        println!(
            "[Build] Warning: Creating [MAN_TARGET_DIR] at {:?}",
            &out_dir
        );

        fs::create_dir_all(&out_dir)?;
    }

    let cmd = cli::Cli::command();

    gen_manpage_recursive(&cmd, &out_dir)?;

    Ok(())
}
