// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

pub type Result<T> = std::result::Result<T, Error>;

pub enum Error {
    Io(std::io::Error),
    Clap(clap::error::Error),
    Toml(toml::de::Error),
    Tera(tera::Error),
    Reqwest(reqwest::Error),
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl From<&str> for Error {
    fn from(e: &str) -> Self {
        Self::Clap(clap::error::Error::raw(clap::error::ErrorKind::Format, e))
    }
}

impl From<String> for Error {
    fn from(e: String) -> Self {
        Self::Clap(clap::error::Error::raw(clap::error::ErrorKind::Format, e))
    }
}

impl From<toml::de::Error> for Error {
    fn from(e: toml::de::Error) -> Self {
        Self::Toml(e)
    }
}

impl From<tera::Error> for Error {
    fn from(e: tera::Error) -> Self {
        Self::Tera(e)
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Self::Reqwest(e)
    }
}

impl From<Error> for clap::Error {
    fn from(e: Error) -> Self {
        match e {
            Error::Io(e) => Self::raw(clap::error::ErrorKind::Io, e),
            Error::Clap(e) => e,
            Error::Toml(e) => Self::raw(clap::error::ErrorKind::Io, e),
            Error::Tera(e) => Self::raw(clap::error::ErrorKind::Io, e),
            Error::Reqwest(e) => Self::raw(clap::error::ErrorKind::Io, e),
        }
    }
}
