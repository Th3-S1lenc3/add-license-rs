// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use clap::Parser;
use directories::ProjectDirs;

const URL: &str = "https://gitlab.com/Th3-S1lenc3/add-license-rs/-/raw/master";
const LICENSES_DIR: &str = "licenses";
const TEMPLATES_DIR: &str = "templates";
const DOCUMENTATION_DIR: &str = "docs";
const MANIFEST_FILE: &str = "manifest.toml";

mod add;
mod cli;
mod doc;
mod error;
mod initialize;
mod licenses;
mod list;

use add::add;
use cli::{Cli, Commands, DocCommands};
use initialize::initialize;
use list::list;

fn project_dirs() -> Option<ProjectDirs> {
    ProjectDirs::from("com.gitlab", "th3-s1lenc3", "add-license-rs")
}

fn main() -> Result<(), std::io::Error> {
    let cli = Cli::parse();

    match &cli.command {
        Commands::Add(args) => match add(&args.license, &args.output) {
            Ok(k) => k,
            Err(e) => clap::Error::from(e).print()?,
        },
        Commands::Init => match initialize() {
            Ok(k) => k,
            Err(e) => clap::Error::from(e).print()?,
        },
        Commands::List => match list() {
            Ok(k) => k,
            Err(e) => clap::Error::from(e).print()?,
        },
        Commands::Doc(args) => match &args.command {
            DocCommands::Gen(a) => match doc::generate(a.verbose) {
                Ok(k) => k,
                Err(e) => clap::Error::from(e).print()?,
            },
            DocCommands::Show => match doc::show() {
                Ok(k) => k,
                Err(e) => clap::Error::from(e).print()?,
            },
            DocCommands::Init => match doc::initialize() {
                Ok(k) => k,
                Err(e) => clap::Error::from(e).print()?,
            },
        },
    }

    Ok(())
}
