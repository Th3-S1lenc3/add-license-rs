// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use crate::error::Result;
use crate::licenses::{self, License};

pub fn list() -> Result<()> {
    let licenses: Vec<License> = licenses::parse()?;

    for license in licenses {
        println!("{}: {}", license.spdx_id, license.title);
    }

    Ok(())
}
