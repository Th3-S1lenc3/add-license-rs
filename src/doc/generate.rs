// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use serde_json::value::{to_value, Value};
use std::collections::HashMap;
use std::fs::{self, File};
use std::path::{Path, PathBuf};
use tera::{Context, Tera};

use std::error::Error as StdError;

use crate::error::Result;
use crate::licenses::{self, License};
use crate::{project_dirs, DOCUMENTATION_DIR, TEMPLATES_DIR};

fn do_nothing_filter(value: &Value, _: &HashMap<String, Value>) -> tera::Result<Value> {
    let s = tera::try_get_value!("do_nothing_filter", "value", String, value);
    Ok(to_value(&s).unwrap())
}

pub fn generate(verbose: bool) -> Result<()> {
    if let Some(project_dir) = project_dirs() {
        let data_dir_path: &Path = project_dir.data_dir();
        let templates_dir_path: PathBuf = data_dir_path.join(TEMPLATES_DIR);
        let documentation_dir_path: PathBuf = data_dir_path.join(DOCUMENTATION_DIR);

        if !templates_dir_path.exists() {
            return Err(format!(
                "Cannot find templates dir at: {}. Try running doc init",
                templates_dir_path.display()
            )
            .into());
        }

        if !documentation_dir_path.exists() {
            if verbose {
                println!(
                    "[Warning] {} does not exist. Creating it...",
                    documentation_dir_path.display()
                );
            }
            match fs::create_dir_all(&documentation_dir_path) {
                Ok(r) => r,
                Err(e) => {
                    return Err(format!("Could not create documentation directory: {}", e).into())
                }
            };
        }

        let templates: Tera = {
            let mut tera = Tera::new(format!("{}/**/*", templates_dir_path.display()).as_str())?;

            tera.autoescape_on(vec!["html", ".sql"]);
            tera.register_filter("do_nothing", do_nothing_filter);
            tera
        };

        let licenses: Vec<License> = licenses::parse()?;

        let mut context = Context::new();
        context.insert("doc_dir", &documentation_dir_path.to_str().unwrap());
        context.insert("licenses", &licenses);

        let file_path = documentation_dir_path.join("index.html");

        if file_path.exists() & verbose {
            println!(
                "[Warning] \"{}\" already exists, overwriting it.",
                file_path.display(),
            );
        }

        let mut f = File::create(file_path)?;

        match templates.render_to("index.html", &context, &mut f) {
            Ok(r) => r,
            Err(e) => {
                println!("{:?}", e.source().unwrap());
                return Err(e.into());
            }
        };

        let license_doc_dir_path = documentation_dir_path.join("licenses");

        if !license_doc_dir_path.exists() {
            if verbose {
                println!(
                    "[Warning] {} does not exist. Creating it...",
                    license_doc_dir_path.display()
                );
            }
            match fs::create_dir_all(&license_doc_dir_path) {
                Ok(r) => r,
                Err(e) => {
                    return Err(
                        format!("Could not create license documentation directory: {}", e).into(),
                    )
                }
            };
        }

        for l in &licenses {
            let mut context = Context::new();
            context.insert("doc_dir", &documentation_dir_path.to_str().unwrap());
            context.insert("licenses", &licenses);
            context.insert("license", &l);

            let file_path = license_doc_dir_path.join(format!("{}.html", &l.spdx_id));

            if file_path.exists() & verbose {
                println!(
                    "[Warning] \"{}\" already exists, overwriting it.",
                    file_path.display(),
                );
            }

            let mut f = File::create(file_path)?;

            match templates.render_to("license.html", &context, &mut f) {
                Ok(r) => r,
                Err(e) => {
                    println!("{:?}", e.source().unwrap());
                    return Err(e.into());
                }
            };
        }

        println!("Done.");
    } else {
        return Err("Failed to load project directories".into());
    }

    Ok(())
}
