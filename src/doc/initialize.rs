// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use serde::Deserialize;
use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};

use crate::error::Result;
use crate::{project_dirs, DOCUMENTATION_DIR, MANIFEST_FILE, TEMPLATES_DIR, URL};

#[derive(Deserialize)]
struct Templates {
    files: Vec<String>,
}

pub fn initialize() -> Result<()> {
    if let Some(project_dir) = project_dirs() {
        let data_dir_path: &Path = project_dir.data_dir();
        let templates_dir_path: PathBuf = data_dir_path.join(TEMPLATES_DIR);
        let documentation_dir_path: PathBuf = data_dir_path.join(DOCUMENTATION_DIR);

        println!("Checking for templates directory...");
        if templates_dir_path.exists() {
            println!("Found templates directory.");
        } else {
            println!("Templates directory does not exist. Creating it...");
            fs::create_dir_all(&templates_dir_path)?;
        }

        let manifest_file = templates_dir_path.join(MANIFEST_FILE);

        println!("Checking for manifest...");
        if manifest_file.exists() {
            println!("Found manifest.");
        } else {
            println!("Downloading Templates Manifest...");

            let templates_url = format!("{URL}/{TEMPLATES_DIR}/{MANIFEST_FILE}");
            let body = reqwest::blocking::get(&templates_url)?.text().unwrap();

            println!("Writing manifest file...");
            fs::write(&manifest_file, &body)?;
        }

        let mut file = fs::File::open(manifest_file)?;
        let mut file_content = String::new();
        let _bytes_read = file.read_to_string(&mut file_content).unwrap();

        let t: Templates = toml::from_str(&file_content)?;

        let mut all_files = true;

        println!("Checking all files are present...");
        for f in &t.files {
            let file_path = templates_dir_path.join(f);

            if !file_path.exists() {
                all_files = false;

                let file_url = format!("{URL}/{TEMPLATES_DIR}/{f}");

                println!("Downloading File: {f}");

                let body = reqwest::blocking::get(file_url)?.text().unwrap();

                println!("Writing \"{f}\" to file...");
                fs::write(&file_path, &body)?;
            }
        }

        if all_files {
            println!("Found all files.");
        }

        println!("Checking for documentation directory...");
        if documentation_dir_path.exists() {
            println!("Found documentation directory.");
        } else {
            println!("Documentation directory does not exist. Creating it...");
            fs::create_dir_all(&documentation_dir_path)?;
        }

        let stylesheet_src_path = templates_dir_path.join("style.css");
        let stylesheet_path = documentation_dir_path.join("style.css");

        println!("Checking for documentation stylesheet...");
        if stylesheet_path.exists() {
            println!("Found stylesheet.");
        } else {
            println!("Cannot find stylesheet copying it from templates directory...");
            fs::copy(&stylesheet_src_path, &stylesheet_path)?;
        }

        println!("Done.");
    } else {
        return Err("Failed to load project directories".into());
    }

    Ok(())
}
