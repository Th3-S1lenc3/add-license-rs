// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use std::path::{Path, PathBuf};

use crate::error::Result;
use crate::{project_dirs, DOCUMENTATION_DIR};

pub fn show() -> Result<()> {
    if let Some(project_dir) = project_dirs() {
        let data_dir_path: &Path = project_dir.data_dir();
        let documentation_dir_path: PathBuf = data_dir_path.join(DOCUMENTATION_DIR);

        let url = format!("{}/index.html", documentation_dir_path.display());

        webbrowser::open(&url)?;
    } else {
        return Err("Failed to load project directories".into());
    }

    Ok(())
}
