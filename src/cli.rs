// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use clap::{Args, Parser, Subcommand};
use std::env;

/// A command line program to add a user specified license to a given directory or the current working directory [if not provided]
#[derive(Parser)]
#[clap(author, version,about, long_about = None)]
#[clap(propagate_version = true)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Add license to current dir or specified directory
    Add(AddArgs),
    /// Run initial setup
    Init,
    /// List Supported Licenses
    List,
    /// Documentation Commands
    Doc(DocArgs),
}

#[derive(Args)]
pub struct AddArgs {
    /// License to add
    #[clap(value_parser)]
    pub license: String,
    /// Where to write the license. If not provided, current working directory will be used.
    #[clap(short, long, default_value_t = env::current_dir().unwrap().display().to_string(), value_parser)]
    pub output: String,
}

#[derive(Args)]
pub struct DocArgs {
    #[clap(subcommand)]
    pub command: DocCommands,
}

#[derive(Subcommand)]
pub enum DocCommands {
    /// Generate Documentation for licenses
    Gen(GenArgs),
    /// Open Documentation in default browser
    Show,
    /// Run initial setup
    Init,
}

#[derive(Args)]
pub struct GenArgs {
    /// Use verbose output
    #[clap(short, long, value_parser)]
    pub verbose: bool,
}
