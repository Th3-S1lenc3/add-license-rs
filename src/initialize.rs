// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use std::fs;
use std::path::{Path, PathBuf};

use crate::error::Result;
use crate::licenses::{self, License};
use crate::{project_dirs, LICENSES_DIR, MANIFEST_FILE, URL};

pub fn initialize() -> Result<()> {
    if let Some(project_dir) = project_dirs() {
        let data_dir_path: &Path = project_dir.data_dir();
        let licenses_dir: PathBuf = data_dir_path.join(LICENSES_DIR);

        println!("Checking for license directory...");
        if licenses_dir.exists() {
            println!("Found license directory.");
        } else {
            println!("License directory does not exist. Creating it...");
            fs::create_dir_all(&licenses_dir)?;
        }

        let manifest_file = licenses_dir.join(MANIFEST_FILE);

        println!("Checking for manifest...");
        if manifest_file.exists() {
            println!("Found manifest.");
        } else {
            println!("Downloading license manifest...");

            let manifest_url = format!("{URL}/{LICENSES_DIR}/{MANIFEST_FILE}");
            let body = reqwest::blocking::get(&manifest_url)?.text().unwrap();

            println!("Writing manifest file...");
            fs::write(&manifest_file, &body)?;
        }

        let licenses: Vec<License> = licenses::parse()?;

        let mut all_licenses = true;

        println!("Checking for licenses...");
        for li in &licenses {
            let license_file = licenses_dir.join(&li.license_file);

            if !license_file.exists() {
                all_licenses = false;
                let license_file_url = format!("{URL}/{LICENSES_DIR}/{}", &li.license_file);
                println!("Downloading License: {}...", &li.license_file);
                let body = reqwest::blocking::get(license_file_url)?.text().unwrap();

                println!("Writing License: {} to file...", &li.license_file);
                fs::write(&license_file, &body)?;
            }
        }

        if all_licenses {
            println!("Found licenses.");
        }

        Ok(())
    } else {
        Err("Failed to load project directories".into())
    }
}
