// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use serde::{Deserialize, Serialize};
use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};

use crate::error::Result;
use crate::{project_dirs, LICENSES_DIR, MANIFEST_FILE};

#[derive(Serialize, Deserialize, Debug)]
pub struct License {
    pub title: String,
    pub description: String,
    pub permissions: Vec<String>,
    pub conditions: Vec<String>,
    pub limitations: Vec<String>,
    pub spdx_id: String,
    pub license_file: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Licenses {
    licenses: Vec<License>,
}

pub fn parse() -> Result<Vec<License>> {
    if let Some(project_dir) = project_dirs() {
        let data_dir_path: &Path = project_dir.data_dir();
        let license_dir: PathBuf = data_dir_path.join(LICENSES_DIR);
        let manifest_file = license_dir.join(MANIFEST_FILE);

        if !manifest_file.exists() {
            return Err("Cannot read license manifest. Try running the init subcommand.".into());
        }

        let mut file = fs::File::open(manifest_file)?;
        let mut file_content = String::new();
        let _bytes_read = file.read_to_string(&mut file_content).unwrap();

        let l: Licenses = toml::from_str(&file_content)?;

        Ok(l.licenses)
    } else {
        Err("Failed to load project directories".into())
    }
}
