// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use path_absolutize::Absolutize;

use std::fs;
use std::path::{Path, PathBuf};

use crate::error::Result;
use crate::licenses::{self, License};
use crate::project_dirs;

pub fn add(license: &str, to: &str) -> Result<()> {
    if let Some(project_dir) = project_dirs() {
        let data_dir_path: &Path = project_dir.data_dir();
        let licenses_dir: PathBuf = data_dir_path.join("licenses");

        let licenses: Vec<License> = licenses::parse()?;

        let mut license_files: Vec<String> = Vec::new();

        for l in licenses {
            if l.spdx_id.to_lowercase() == license.to_lowercase()
                || l.title.to_lowercase() == license.to_lowercase()
            {
                license_files.push(l.license_file);
            }
        }

        if license_files.is_empty() {
            return Err(format!("Could not find provided license: {license}").into());
        }

        for l in license_files {
            let license_file_path: PathBuf = licenses_dir.join(&l);

            if !license_file_path.exists() {
                return Err(format!("Could not find provided license: {license}").into());
            }

            let fp = Path::new(&to).absolutize().unwrap();
            let mut file_path = fp.to_path_buf();

            file_path.push("LICENSE");

            let _ = fs::copy(license_file_path, file_path)?;
        }

        Ok(())
    } else {
        Err("Failed to load project directories".into())
    }
}
